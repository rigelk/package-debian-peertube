class PeerTube < FPM::Cookery::Recipe
  description 'Federated video streaming platform using BitTorrent directly in the browser built with Angular.'

  name     "peertube"
  version  "1.2.1"
  maintainer 'Rigel Kent <par@rigelk.eu>'
  revision 0
  homepage 'https://joinpeertube.org/'
  source   'https://github.com/Chocobozzz/PeerTube',
    :with => 'git',
    :tag	=> 'v1.2.1'
  arch     'amd64'
  license  'AGPL-3.0'

  case platform
    when :centos, :redhat
      then
      build_depends 'nodejs (>= 8)', 'g++ (>= 5.0)', 'yarn (>= 1.5.1)'
      depends       'nodejs (>= 8)', 'ffmpeg (>= 3)', 'openssl', 'postgresql', 'redis', 'yarn (>= 1.5.1)', 'git', 'make'
    when :debian, :ubuntu
      then
      build_depends 'nodejs (>= 8)', 'g++ (>= 5.0)', 'yarn (>= 1.5.1)'
      depends       'nodejs (>= 8)', 'ffmpeg (>= 3)', 'openssl', 'postgresql', 'redis-server', 'yarn (>= 1.5.1)', 'git', 'g++ (>= 5.0)', 'make'
      section       'web'
  end

  provides  'peertube'
  replaces  'peertube'
  conflicts 'peertube'

  pre_install    'pre-install'
  post_install   'post-install'
  pre_uninstall  'pre-uninstall'
  post_uninstall 'post-uninstall'

  def build
    sh 'yarn install --pure-lockfile'
    sh 'npm run build'
    Dir.glob('**/*').reject{|k| ["CREDITS.md", "LICENSE", "client/dist/",
"client/package.json", "dist", "scripts", "tsconfig.json", "FAQ.md",
"README.md", "client/yarn.lock", "config", "package.json", "support",
"yarn.lock"].each {|l| k.include? l }}.each do |f|
      FileUtils.rm_rf f
    end
    Dir.glob('**/node_modules/**/*') do |f|
      FileUtils.rm_rf f
    end
  end
  def install
    # startup script
    lib('systemd/system').install workdir('peertube.service')

    # config files
    etc('peertube').install Dir['config/production.yaml.example']
    etc('peertube').install Dir['config/default.yaml']

    # default site
    var('www/peertube').install Dir['*']

    # documentation
    share('doc/peertube').install Dir['support/doc/*']

    # remote tools (https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/tools.md#remote-tools)
    bin.install 'peertube'
  end
end

