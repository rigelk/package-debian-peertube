# PeerTube Debian Package

This is a **community package**, maintained outside of the PeerTube
organisation. While it should be quite stable, this package comes with no
guarantee of support.

## Building the package

`$ rake`

That's it! You expected something more? Sorry to disappoint you ;)

## Bonus

The definition can actually generate more than just Debian Stretch packages, it
can generate ArchLinux, Centos, Fedora, Ubuntu packages too, although I have
not tested them.

## License
[AGPLv3](https://choosealicense.com/licenses/agpl-3.0/)
